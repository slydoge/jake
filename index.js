Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};
String.prototype.nopunct = function(){
	return this.replace(/[^A-Za-z0-9\s]/g,"").replace(/\s{2,}/g, " ");
}
class Room {
    constructor(properties) {
        this.name = properties.name;
        this.description = properties.description;
        this.items = properties.items || [];
        this.directions = properties.directions || {};
        this.locked = properties.locked || false;
        this.props = properties.props || [];
        this.visited = false;
        this.status = properties.status || undefined;
        this.events = {};
        this.resetStatus = properties.resetStatus;
        if (this.resetStatus) {
            this.on('left', () => {
                this.status = properties.status;
            });
        }
    }
    itemList(){
    	//var items = this.items.map(o => o.name).join(', ');
    	var items = "";
    	for (var i = 0; i < this.items.length; i++) {
    		items += this.items[i].name;
    		if (i<this.items.length-2) items+= ", ";
    		else if (i<this.items.length-1) items+= " and ";
    	}
    	return items;
    }
    introduce(){
        this.emit('entered');
        this.visited = true;

        $(".location").text(this.name);
    	$(".field").append(`
    		<p>
	    		${this.status=='dark'?"Its pich-black here. You fumble in the dark, trying to get a grip of a wall to avoid falling.":this.description}

	    		${this.items.length>0&&this.status!='dark'?'Looking around, you see ' + this.itemList() + '.':'There seems to be nothing of value here.'}
	    	</p>
    	`);
    }
    listItems(){
    	$(".field").append(`
    		<p>
    			${this.items.length>0&&this.status!='dark'?'Looking around, you notice a few items: ' + this.itemList() + '.':'There seems to be nothing of value here.'}
	    	</p>
    	`);
    }
    listProps(){
        var propString = "";

        if (this.props.length<=0 || this.status == 'dark') {
            propString += "There's nothing of note around here.";
        }
        else{
            if (this.props.length==1) propString += "Of note is ";
            else propString += "Of note are ";
            for (var i = 0; i < this.props.length; i++) {
                propString += this.props[i].name;
                if (i<this.props.length-2) propString+= ", ";
                else if (i<this.props.length-1) propString+= " and ";
            }
        }

        $(".field").append(`
            <p>
                You throw your gaze around the stuff in the room. ${propString}.
            </p>
        `);
    }
    findItem(itemName){
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].tags.includes(itemName)) return this.items[i];
        }
        return false;
    }
    findProp(propName){
        for (var i = 0; i < this.props.length; i++) {
            if (this.props[i].tags.includes(propName)) return this.props[i];
        }
        return false;
    }
    setDirection(dir, room){
    	this.directions[dir] = room;
    }
    on(type, func){
        this.events[type] = func;
    }
    emit(type){
        if(this.events[type]) this.events[type](this);
    }
    affectRoom(func){
        if (func != undefined) func(this);
    }
}

class Item {
    constructor(properties) {
        this.name = properties.name;
        this.type = properties.type;
        this.description = properties.description;
        this.key = properties.key;
        this.used = properties.used;
        this.recipe = properties.recipe;
        this.craft = properties.craft;
        this.action = properties.action;
        this.events = {};
        this.tags = properties.tags || [];
        this.affectRoom = properties.affectRoom;
        this.destroyIngredients = properties.destroyIngredients;
    }
    useProp(){
        if (this.type=='prop') {
            $(".field").append(`<p>${this.action}</p>`)
        }
    }
    on(type, func){
        this.events[type] = func;
    }
    emit(type){
        if(this.events[type]) this.events[type](this);
    }
}

class Player {
    constructor(properties) {
        this.name = properties.name;
        this.currentRoom = properties.room;
        this.totalMoves = 0;

        this.inventory = [
            new Item({
                name: 'pocket lint',
                type: 'fuel',
                tags: ['lint'],
                description: `If there's one thing you will never run out of, its this.`
            })
        ];
    }
    pickUp(item){
    	if (!item) {
	    	$(".field").append(`<p>You reach out towards the item you saw. As you approach and squint you realize there was no item.</p>`);
    		return;
    	}

    	this.inventory.push(item);
        item.emit('pickedup');
    	this.currentRoom.items.remove(item);

    	$(".field").append(`
    		<p>
    			You picked up ${item.name}. ${item.description}
    		</p>
    	`);
    }
    logAction(action){
        this.totalMoves++;
        $('.moves').text('Moves: ' + this.totalMoves);
	    $(".field").append('>' + action);
    }
    enter (room){
        if (room.locked && this.inventory.filter(obj => { return obj.key == room.locked}).length <= 0) {
            $(".field").append(`
                <p>
                    This room is locked.
                </p>
            `);
            return;
        }
        else if (room.locked && this.inventory.filter(obj => { return obj.key == room.locked}).length > 0) {
            $(".field").append(`
                <p>
                    This room is locked. You search your pockets for a key and find one that fits. Carefully, you unlock the door and enter.
                </p>
            `);
            room.locked = false;
        }
        this.currentRoom.emit('left');
        this.currentRoom = room;
        this.currentRoom.introduce();
    }
    itemList(){
        //var items = this.items.map(o => o.name).join(', ');
        var items = "";
        for (var i = 0; i < this.inventory.length; i++) {
            items += this.inventory[i].name;
            if (i<this.inventory.length-2) items+= ", ";
            else if (i<this.inventory.length-1) items+= " and ";
        }
        return items;
    }
    listItems(){
        $(".field").append(`
            <p>
                You stick your hand into your pockets and find ${this.inventory.length>0?this.itemList():'absolutely nothing. Not even pocket lint'}.
            </p>
        `);
    }
    use(item){
        if (!item.used) {
            $(".field").append(`<p>You frantically turn ${item.name} around in your hands, realizing you forgot how to use it.</p>`);
            return;
        }
        $(".field").append(`<p>${item.used}</p>`);

        if (item.affectRoom != undefined) {
            this.currentRoom.affectRoom(item.affectRoom);
        }

        if (item.type=="food" || item.type == "consumable") this.inventory.remove(item);
    }
    findItem(itemName){
        for (var i = 0; i < this.inventory.length; i++) {
            if (this.inventory[i].tags.includes(itemName)) return this.inventory[i];
        }
        return false;
    }
    craft(craftables, item1, item2){
        for (var i = 0; i<craftables.length; i++) {
            if (craftables[i].recipe.item1 == item1.name && craftables[i].recipe.item2 == item2.name) {
                $(".field").append(`<p>${craftables[i].craft}</p>`);

                if (craftables[i].destroyIngredients) {
                    this.inventory.remove(item1);
                    this.inventory.remove(item2);
                }
                this.inventory.push(craftables[i]);

                return;
            }
            else{
                $(".field").append(`<p>You squeeze ${item1.name} and ${item2.name} together. Nothing happens. Try using some glue next time.</p>`);
            }
        }
    }
}

class Recipe {
    constructor(properties) {
        this.item1 = properties.item1;
        this.item = properties.item2;
        this.result = properties.result;
    }
}

var craftables = [];

craftables.push(new Item({
    name: 'an old lighter',
    type: 'tool',
    description: 'This lighter stinks of kerosene. Its got enough fuel for now.',
    destroyIngredients: true,
    recipe: {
        item1: 'a small can of kerosene',
        item2: 'a dented lighter'
    },
    tags: ['lighter', 'old lighter'],
    used: 'You flip the ligher open and swipe your finger down the spark wheel. After a few attemps, a faint flame appears, quickly rising to a lively blaze. You feel strangely relieved.',
    affectRoom: function(e){
        e.status = undefined;
    },
    craft: 'The rancid smell of old kerosene fills the air as you pour whatever was left from the can into the lighter.'
}));

var boardedWindow = new Item({
    name: 'boarded up window',
    type: 'prop',
    tags: ['window', 'boarded window'],
    description: "This window is nailed shut. There is no way you can pry these huge boards off with your bare hands.",
    action: "You pause and stare intently at the window. The bleak rays of sunlight pulse with each passing cloud."
});

//add boxes as props

var livingRoom = new Room({
    name: "Living room",
    description: "You find yourself in a room full of boxes. Although there is nothing left but boxes and a big chandelier, you feel this may have once been a living room. Some boxes probably have goodies in them.",
    items: [
        new Item({
            name: 'a small can of kerosene',
            type: 'tool',
            tags: ['small can of kerosene', 'can of kerosene', 'kerosene', 'kerosene can'],
            description: `This isn't your go-to solution for starting fires, but it will do.`
        }),
        new Item({
            name: 'a pack of saltine crackers',
            type: 'food',
            tags: ['pack of saltine crackers', 'saltine crackers', 'crackers', 'pack of crackers'],
            description: 'Who knows how long these have been here. Better make sure they still taste good.',
            used: 'You stuff your face full of dry, crunchy crackers. Before you realize, you finish the whole pack.'
        }),
        new Item({
            name: 'a blank notepad',
            type: 'note',
            tags: ['blank notepad', 'notepad'],
            description: 'This notepad seems to be entirely blank. Or maybe its full of invisible ink?' 
        })
    ],
    props: [
        new Item({
            name: "a dusty chandelier",
            tags: ['dusty chandelier', 'chandelier'],
            type: 'prop',
            description: "This chandelier is many times older than you, thats for sure.",
            action: "You look up at the chandelier. No matter how long you keep staring at it, it doesn't seem to want to fall. Oh well."
        })
    ]
});

var closet = new Room({
    name: "Closet",
    description: "You find yourself in a small, cramped space. The smell of damp dust is overpowering.",
    locked: 'closet_key',
    status: 'dark',
    resetStatus: true,
    items: [
        new Item({
            name: 'a snapped broom handle',
            type: 'tool',
            tags: ['snapped broom handle', 'broom handle', 'handle'],
            description: 'The broom is nowhere to be seen. Maybe some day you can reunite it with the handle.'
        }),
        new Item({
            name: 'a crumpled piece of paper',
            type: 'note',
            tags: ['crumpled piece of paper', 'piece of paper', 'paper', 'piece'],
            description: 'Its a piece of what probably used to be a notebook. Its smeared with dust and ink.',
            used: `
                You flip the piece of paper around in your hands and notice something on the back.<br><br>

                Its a hand drawn picture of a dog. Or maybe a cat. You're not quite sure of the artist's direction here. Under the drawing you see some text. It says "Leslie".
            `
        })
    ]
});

var street = new Room({
    name: "Street",
    description: "You're free.",
    locked: 'street_key'
});

var lobby = new Room({
    name: "Lobby",
    description: "You find yourself in a small, poorly lit room. Dim rays of sunshine filter through the large boarded up window.",
    directions: {
        north: street
    },
    props: [ boardedWindow ]
});

var diningRoom = new Room({
    name: "Dining room",
    description: "You find yourself in a large, spacious room. In the center there is a big table covered by a drabby tablecloth. You sure wish you could sit down and have a meal, but there's no food here, only chairs. And you don't eat chairs.",
    directions: {
        west: lobby
    }
});

lobby.setDirection('east', diningRoom);

var hall = new Room({
    name: "Hallway",
    description: "You find yourself in a long, gloomy hallway. The walls are stained with grime and dust and the air smells damp.",
    directions: {
        north: lobby,
        east: closet,
        south: livingRoom
    }
});

lobby.setDirection('south', hall);
closet.setDirection('west', hall);
livingRoom.setDirection('north', hall);

var startingRoom = new Room({
    name: "Strange room",
    description: "You find yourself in a strange room. There is barely any light filtering through the single boarded up window. To the right of you is a door.",
    items: [
        new Item({
            name: 'a rusty, pitted knife',
            type: 'tool',
            tags: ['rusty pitted knife', 'rusty knife', 'pitted knife', 'knife'],
            description: 'This "knife" is merely a blade. The handle was  broken off a long time ago.'
        }),
        new Item({
            name: 'a dented lighter',
            type: 'tool',
            tags: ['dented lighter', 'lighter'],
            description: 'This lighter is all out of fuel.'
        })
    ],
    directions: {
        east: hall,
    },
    props: [ boardedWindow ]
});

hall.setDirection('west', startingRoom);

var player = new Player({
	name: 'Jake',
	room: startingRoom
});

$(document).ready(function() {
	startingRoom.introduce();

	$('#input').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            var msg = $("#input").val().toLowerCase();
            $("#input").val('');

            player.logAction(msg);

            if (msg.startsWith("pick up")) {
             	msg = msg.replace("pick up", "").nopunct().trim();
             	//maybe pick up by tags instead, hmm?
                if (player.currentRoom.items.find(x => x.name.nopunct() === msg)) player.pickUp(player.currentRoom.items.find(x => x.name.nopunct() === msg));
                else player.pickUp(player.currentRoom.findItem(msg));
            }
            else if (msg.startsWith("go")) {
             	msg = msg.replace("go", "").nopunct().trim().replace('right', 'east').replace('forward', 'north').replace('left', 'west').replace('backward', 'south');

             	if (player.currentRoom.directions[msg]) player.enter(player.currentRoom.directions[msg]);
             	else $(".field").append(`<p>There seems to be no exit this way.</p>`);
            }
            else if (msg.startsWith("drop")) {
                msg = msg.replace("drop", "").nopunct().trim();

                var subjectItem = player.inventory.find(x => x.name.nopunct() === msg);

                if (subjectItem) {
                    player.inventory.remove(subjectItem);
                    player.currentRoom.items.push(subjectItem);

                    $(".field").append(`<p>You pull ${subjectItem.name} out of your pocket and watch it make it's way to the floor below.</p>`);
                }
                else{
                    subjectItem = player.findItem(msg);
                    if (subjectItem) {
                        player.inventory.remove(subjectItem);
                        player.currentRoom.items.push(subjectItem);

                        $(".field").append(`<p>You pull ${subjectItem.name} out of your pocket and watch it make it's way to the floor below.</p>`);
                    }
                    else{
                        $(".field").append(`<p>You turn your pockets inside out but nothing falls out. Go figure.</p>`);
                    }
                }
            }
            else if (msg.startsWith("use")) {
                msg = msg.replace("use", "").nopunct().trim();

                if (msg.includes(' on ')) {
                    msg = msg.split(' on ');

                    var item1 = player.inventory.find(x => x.name.nopunct() === msg[0]);
                    var item2 = player.inventory.find(x => x.name.nopunct() === msg[1]);

                    if (!item1) {
                        item1 = player.findItem(msg[0]);
                        if (!item1) {
                            $(".field").append(`
                                <p>You briefly dream of all kinds of cool things you could make with "${msg[0]}". You feel a bit disappointed that you dont have it right now.</p>
                            `);
                            return;
                        }
                    }
                    if (!item2) {
                        item2 = player.findItem(msg[1]);
                        if (!item2) {
                            $(".field").append(`
                                <p>You briefly dream of all kinds of cool things you could make with "${msg[1]}". You feel a bit disappointed that you dont have it right now.</p>
                            `);
                            return;
                        }
                    }

                    if (item1==item2) {
                        $(".field").append(`
                            <p>You pull out ${item1.name} and then search your pockets, realizing you only have one of these. Oops.</p>
                        `);
                        return;
                    }

                    player.craft(craftables, item1, item2);
                }
                else{
                    if (player.inventory.find(x => x.name.nopunct() === msg)) {
                        player.use(player.inventory.find(x => x.name.nopunct() === msg));
                    }
                    else if (player.findItem(msg)) {
                        player.use(player.findItem(msg));
                    }
                    else{
                        $(".field").append(`
                            <p>You attempt to make use of "${msg}". After failing to catch grip of your imagination, you give up.</p>
                        `);
                    }

                }
            }
            else if (msg.startsWith("admire")) {
                msg = msg.replace("admire", "").nopunct().trim();

                if (player.currentRoom.props.find(x => x.name.nopunct() === msg)) {
                    player.currentRoom.props.find(x => x.name.nopunct() === msg).useProp();
                }
                else if (player.currentRoom.findProp(msg)) {
                    player.currentRoom.findProp(msg).useProp();
                }
                else{
                    $(".field").append(`
                        <p>You stare blankly into the distance. After musing for a couple moments you return to the where you left off.</p>
                    `);
                }
            }
            else if (msg.startsWith("inventory")) {
                player.listItems();
            }
            else if (msg.startsWith("look around")) {
                player.currentRoom.listProps();
            }
            else if (msg.startsWith("search")) {
            	player.currentRoom.listItems();
            }
            else if (msg.startsWith("help")) {
		    	$(".field").append(`
		    		<p>
		    			You rummage in your pockets and find a tiny crumpled paper. Straightening it out, you see some writing on it:<br><br>
		    			pick up [object] - pick up an object<br>
                        use [object] - attempt to operate an object<br>
                        use [object] on [object] - DIY has never been easier<br>
                        drop [object] - litter the floor with your goodies<br>
                        admire [prop] - can't miss that opportunity<br>
		    			go [direction] - leave in a specified direction<br>
		    			search - look around for usable objects<br>
                        inventory - verify the contents of your pockets<br>
		    			help - pray for the best 
		    		</p>
		    	`);
            }
            else $(".field").append(`<p>You quietly mumble "${msg}"... Nothing happens.</p>`);

            $('.field').animate({scrollTop: $('.field').prop('scrollHeight')});
        }
        event.stopPropagation();
    });

    var keyPickedUp = false;
    var keyDropped = false;

    hall.on('entered', function(e){
        if (e.visited && !keyDropped) {
            e.items.push(new Item({
                name: 'a crooked key',
                type: 'key',
                key: 'closet_key',
                tags: ['crooked key', 'key'],
                description: `Its a key that has been bent out of shape with it's frequent usage and age.`
            }));
            keyDropped = true;
            e.items[e.items.length-1].on('pickedup', function(e){
                if (!keyPickedUp) {
                    $(".field").append(`
                        <p>
                            You could swear this hasn't been here before. 
                            You dart your eyes around the room nervously, expecting to see some boogeyman or perhaps a kooky victorian ghost, however theres still nothing but you and your doubts around. 
                            You shrug and shove the key into your pocket.
                        </p>
                    `);
                    keyPickedUp = true;
                }
            });
        }
    });
});	